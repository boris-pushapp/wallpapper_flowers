package lul.omega.quadro.imagpapper.ui.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_blank.view.*

import lul.omega.quadro.imagpapper.R
/**
 * A simple [Fragment] subclass.
 *
 */
class BlankFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_blank, container, false)
        Picasso.with(context).load(arguments!!.getString("imageUrl")).into(v.img_fragment)
        return v
    }

    companion object {
        fun newInstance(url: String): BlankFragment {
            val args: Bundle = Bundle()
            args.putString("imageUrl", url)
            val fragment = BlankFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
