package lul.omega.quadro.imagpapper.ui

import android.Manifest
import android.app.WallpaperManager
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.Toast
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.activity_main.*
import lul.omega.quadro.imagpapper.MyApi
import lul.omega.quadro.imagpapper.R
import lul.omega.quadro.imagpapper.model.Img
import lul.omega.quadro.imagpapper.ui.fragments.BlankFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private val images = ArrayList<String>()
    private lateinit var adapter: MyViewPagerAdapter
    private val api_key = "34d2df1f35229d2da6af01e7d3528e1e7da74ae0853cd48422d69a5a0507bb72"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        adapter = MyViewPagerAdapter(supportFragmentManager)
        viewpager.adapter = adapter
        getImgs()

        btn_dwnld.setOnClickListener(this)
        btn_wllpppr.setOnClickListener(this)
    }

    private fun getImgs() {
        MyApi.create().getImgList(api_key).enqueue(object : Callback<List<Img>> {
            override fun onFailure(call: Call<List<Img>>?, t: Throwable?) {
                Log.d("fail", "fail")
            }

            override fun onResponse(call: Call<List<Img>>?, response: Response<List<Img>>) {
                val objects = ArrayList<Img>(response.body())
                Log.d("kek", objects.toString())
                for (obj in objects)
                    images.add(obj.urls.regular + "/1080x1920")

                adapter.notifyDataSetChanged()
            }
        })
    }

    inner class MyViewPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
        override fun getItem(position: Int): Fragment {
            return BlankFragment.newInstance(images[position])
        }

        override fun getCount(): Int {
            return images.size
        }

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            btn_dwnld.id -> {
                download()
                btn_dwnld.isEnabled = false
            }
            btn_wllpppr.id -> setWall()
        }
    }

    private fun setWall() {
        Picasso.with(this).load(images[viewpager.currentItem]).into(object : Target {
            override fun onBitmapFailed(errorDrawable: Drawable?) {
                Log.d("kek", "failur")

            }

            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                val myWallpaperManager = WallpaperManager.getInstance(applicationContext)
                try {
                    myWallpaperManager.setBitmap(bitmap)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                Log.d("kek", "prepare")
            }

        })
    }

    private fun download() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    50)
        }
        Picasso.with(this).load(images[viewpager.currentItem]).into(object : Target {
            override fun onBitmapFailed(errorDrawable: Drawable?) {
                Log.d("kek", "failur")

            }

            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                Thread(Runnable {
                    var file = File(Environment.getExternalStorageDirectory(), getString(R.string.app_name))
                    if (!file.exists()) {
                        file.mkdirs()
                    }
                    file = File(Environment.getExternalStorageDirectory().getPath() + "/" + getString(R.string.app_name) + "/image" + viewpager.currentItem + ".jpg")
                    try {
                        file.createNewFile()
                        val ostream = FileOutputStream(file)
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream)
                        ostream.flush()
                        ostream.close()
                        Log.d("succes", images[viewpager.currentItem])
                        runOnUiThread {
                            btn_dwnld.isEnabled = false
                            Toast.makeText(this@MainActivity, "Файл загружен", Toast.LENGTH_LONG).show()

                        }

                    } catch (e: IOException) {
                        Log.d("IOException", e.getLocalizedMessage())
                    }
                }).start()

            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                Log.d("kek", "prepare")
            }
        })
    }
}
