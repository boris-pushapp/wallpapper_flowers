package lul.omega.quadro.imagpapper.model


data class ProfileImage(
    val small: String,
    val medium: String,
    val large: String
)